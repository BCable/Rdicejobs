Rdicejobs 1.0.1
===============

**Note: Dice.com's API is no longer publicly available.**

R package that pulls data from Dice.com's JobSearch API documented [here](http://www.dice.com/common/content/util/apidoc/jobsearch.html).

Example maps created with the package here: [Rdicejobs Example](https://bcable.net/analysis-dicejobs.html)

Dependencies (auto-installed with package install): RCurl, rjson

# Installation #

This package requires the "devtools" package available in [CRAN](https://cran.r-project.org/).

## Install devtools ##

Assuming you don't already have devtools installed, run the following:

```
install.packages("devtools")
```

## Install Rdicejobs ##

With devtools installed, it's fairly simple to install Rdicejobs:

```
library(devtools)
install_git("https://gitlab.com/BCable/Rdicejobs.git")
```

# Examples #

```r
library(Rdicejobs)

# Grab job count for the state of Illinois with R as a skill
dice_job_count(skill="R", state="IL")

# Grab job listings in the United States with the text "awful" in the
# job description somewhere
awful_jobs <- dice_job_data(country="US", text="awful")
```
